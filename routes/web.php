<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'home')->name('home');
Route::get('/fetch_form/{type}', 'HybridCtrl@fetch_form')->name('fetch_form');
Route::post('/save_form', 'HybridCtrl@save_form')->name('save_form');
Route::get('/success/{token}', 'HybridCtrl@success')->name('success');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
