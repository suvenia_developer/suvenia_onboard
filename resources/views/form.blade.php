<div class="uk-modal-dialog" style="background-image: url({{ asset('img/bg-1.png') }});">
<button class="uk-modal-close-default" type="button" uk-close></button>
<div class="uk-overlay-pri uk-position-cover"></div>

<div class="row p-4 justify-content-center" uk-height-viewport>
    <div class="col-md-6 mt-4">
        <h1 class="m-0 _modal_title">GET STARTED</h1>
        <h3 class="m-0 _modal_sub">Let’s start creating awesomeness</h3>
        <div class="card p-4 mt-2">
                <form action="{{ route('save_form') }}" method="post" data-post>
                    @csrf
                        <div class="form-group email">
                            <input type="email" name="email" id="" class="form-control" placeholder="Email Address">
                            <p class="help-block m-0"></p>
                        </div>
                        <div class="form-group password">
                            <input type="password" name="password" id="" class="form-control" placeholder="Password">
                            <p class="help-block m-0"></p>
                        </div>
                        <div class="form-group confirm_password">
                            <input type="password" name="confirm_password" id="" class="form-control" placeholder="Confirm Password">
                            <p class="help-block m-0"></p>
                        </div>

                        @if(isset($type) && $type == 'INFLUENCER')
                        <div class="form-group instagram">
                                <input type="url" name="instagram" id="" class="form-control" placeholder="instagram">
                                <p class="help-block m-0"></p>
                        </div>
                       @endif
                       
                       @if(isset($type) && $type == 'DESIGNER')
                        <div class="form-group portfolio">
                            <input type="url" name="portfolio" id="" class="form-control" placeholder="Personal website or portfolio link">
                            <p class="help-block m-0"></p>
                        </div>
                      @endif
                      <input type="hidden" name="type" value="{{ $type }}">
                        <div class="form-group uk-text-center">
                            <button class="btn btn-info">Join Community</button>
                        </div>
            
                </form>
        </div>
    </div>
</div>

</div>