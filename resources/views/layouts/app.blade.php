<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Suvenia - @yield('title')</title>
    <meta name="description" content="">
    @if($utils->device()->isMobile() || $utils->device()->isTablet())
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @endif
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">
     @stack('PAGE_STYLES')
    <link rel="stylesheet" href="{{ mix('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('css/main.css') }}"> 
   
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/main.js') }}"></script>

    @include('partials.notify') 
    @stack('PAGE_SCRIPTS')

</head>

<body>
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    @include('partials.header')

    <div uk-height-viewport="expand: true">
        @yield('content')
    </div>

    @includeIf('partials.foote')

</body>

</html>