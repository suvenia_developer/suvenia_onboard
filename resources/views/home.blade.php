@extends('layouts.app')

@section('title', 'Welcome' )

@push('PAGE_STYLES')
<style>
</style>
@endpush

@section('content')
    <div>
        <div class="uk-cover-container" style="position: relative;" uk-height-viewport>
                <img src="{{ asset('img/bg-1.png') }}" alt="Suvenia Nigeria" uk-cover>
                @if($utils->device()->isMobile() || $utils->device()->isTablet())
                <div class="uk-overlay-default uk-position-cover"></div>
                @endif
                <div class="cont border">
                        <div class="uk-position-bottom">
                            <div class="uk-container uk-container-small mt-4">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h1 class="app-hero m-0">Your Creativity Deserves an Income</h1>
                                        <h3 class="app-hero-sub m-0">Whatever your creative skills are, Suvenia.com now has a rewarding way for you to make earning easy.</h3>
                                        <div class="mt-2">
                                            <ul class="list-unstyled">
                                                <li class="d-inline-block m-1">
                                                    <a href="#SellerPane" class="btn btn-sm btn-white" uk-scroll>I’M A SELLER</a>
                                                </li>
                                                <li class="d-inline-block m-1">
                                                    <a href="#DesignerPane" class="btn btn-sm btn-white" uk-scroll>I’M A DESIGNER</a>
                                                </li>
                                                <li class="d-inline-block m-1">
                                                    <a href="#InfluencerPane" class="btn btn-sm btn-white" uk-scroll>I’M AN INFLUENCER</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 uk-text-center">
                                        <a href="#SellerPane" uk-icon="icon: chevron-down; ratio: 1.5" uk-scroll></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
        </div>
    </div>

    <div class="uk-section" uk-scrollspy="cls: uk-animation-slide-left-small; repeat: true" id="SellerPane">
        <div class="uk-container">
            <div class="row justify-content-center">
                @if($utils->device()->isMobile() || $utils->device()->isTablet())
                <div class="col-md-5 mb-2 uk-text-center">
                @else
                <div class="col-md-5 mb-2">
                @endif
                    <h2 class="app-section-title">BECOME A SELLER</h2>
                    <p class="app-section-text">
                            Whether you are a graphic designer, Photographer, artist or just a creative individual, wouldn’t you like to get paid simply for doing what you already love to do? Suvenia enables thousands of Creatives to sell their designs and ideas on quality product. Simply upload your designs on any of our products and we do everything else till you get paid.
                    </p>
                    <a href="{{ route('fetch_form', ['type'=> 'SELLER']) }}" class="btn btn-sm btn-white __reg_form_trigger">I’M INTERESTED</a>
                </div>
                <div class="col-md-5">
                    <img src="{{ asset('img/seller_ad.png') }}" alt="" width="400" uk-responsive>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-section" uk-scrollspy="cls: uk-animation-slide-right-small; repeat: true" id="DesignerPane">
        <div class="uk-container">
            <div class="row justify-content-center">
                @if($utils->device()->isMobile() || $utils->device()->isTablet())
                   <div class="col-md-5 mb-3 uk-text-center">

                        <h2 class="app-section-title">WORK AS A FREELANCE DESIGNER</h2>
                        <p class="app-section-text">
                                Find amazing clients looking for your services from our community. You can help create designs for others to sell on quality products or work on any other project you want. You can also participate in design contests with other designers in our community powered by Suvenia.
                        </p>
                        <a href="{{ route('fetch_form', ['type'=> 'DESIGNER']) }}" class="btn btn-sm btn-white __reg_form_trigger"">I’M INTERESTED</a>
                    </div>
                    <div class="col-md-5 mb-3 uk-text-center">
                            <img src="{{ asset('img/freelancer_ad.png') }}" alt="" width="400" uk-responsive>
                    </div>
                @else
                
                <div class="col-md-5 mb-3">
                        <img src="{{ asset('img/freelancer_ad.png') }}" alt="" width="400" uk-responsive>
                </div>
                <div class="col-md-5">

                    <h2 class="app-section-title">WORK AS A FREELANCE DESIGNER</h2>
                    <p class="app-section-text">
                            Find amazing clients looking for your services from our community. You can help create designs for others to sell on quality products or work on any other project you want. You can also participate in design contests with other designers in our community powered by Suvenia.
                    </p>
                    <a href="{{ route('fetch_form', ['type'=> 'DESIGNER']) }}" class="btn btn-sm btn-white __reg_form_trigger"">I’M INTERESTED</a>
                </div>

                @endif
                
            </div>
        </div>
    </div>

    <div class="uk-section" uk-scrollspy="cls: uk-animation-slide-left-small; repeat: true" id="InfluencerPane">
        <div class="uk-container">
            <div class="row justify-content-center">
                    @if($utils->device()->isMobile() || $utils->device()->isTablet())
                    <div class="col-md-5 mb-2 uk-text-center">
                    @else
                    <div class="col-md-5 mb-2">
                    @endif

                    <h2 class="app-section-title">MONETIZE YOUR SOCIAL MEDIA</h2>
                    <p class="app-section-text">
                            Enjoy trying new products, taking great pictures and showing them?
Why not earn from them while you’re at it! Whether you have 500 followers or 5 Million followers, Suvenia.com makes it easy for you to earn by creating and sharing content on your own social media channels.
                    </p>
                    <a href="{{ route('fetch_form', ['type'=> 'INFLUENCER']) }}" class="btn btn-sm btn-white __reg_form_trigger">I’M INTERESTED</a>
                </div>

                <div class="col-md-5">
                        <img src="{{ asset('img/influencer_ad.png') }}" alt="" width="400" uk-responsive>
                </div>
                
            </div>
        </div>
    </div>


@endsection

@push('PAGE_SCRIPTS')
@endpush
