@extends('layouts.app')

@section('title', 'Success')

@push('PAGE_STYLES')
<style>
</style>
@endpush

@section('content')
    <section class="uk-cover-container" uk-height-viewport="expand: true">
            <img src="{{ asset('img/bg-1.png') }}" alt="Suvenia Nigeria" uk-cover>
            <div class="uk-overlay-pri uk-position-cover"></div>
        <div class="uk-container uk-position-center">
            <div class="row justify-content-center">
                <div class="col-8 uk-text-center">
                    <p class="m-0 text-white">Thank you for joining our community.</p>
                    <p class="mt-0 text-white">You’ll be invited as one of our first users to try out the new version.</p>
                    <div>
                        <a class="btn btn-white-2" href="https://suvenia.com">Visit suvenia.com</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('PAGE_SCRIPTS')
@endpush
