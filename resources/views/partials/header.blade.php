
<div class="uk-navbar-container middle-nav" uk-sticky="top: 100; animation: uk-animation-slide-top; cls-active: sticky-nav-web">
    <div class="uk-container ">
        <nav class="uk-navbar">
                <div class="uk-navbar-left">

                <a class="uk-navbar-item uk-logo" href="{{ route('home') }}">
                        <img src="{{ asset('img/logo.png') }}" width="100" uk-responsive>
                    </a>
                        
                </div>

             @if($utils->device()->isMobile() || $utils->device()->isTablet())
             <div class="uk-navbar-right">
                    <div class="uk-navbar-item">
                        <a href="javascript:;" uk-icon="icon:menu; ratio: 1.5;" uk-toggle="target: #NavOffcanvas"></a>
                    </div>
             </div>
            @else
            <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav">
                        <li>
                            <a href="https://suvenia.com" target="_blank">
                                Suvenia
                            </a>
                        </li>
                        <li>
                            <a href="#SellerPane" uk-scroll>
                                seller
                            </a>
                        </li>
                        <li>
                            <a href="#DesignerPane" uk-scroll>
                                designer
                            </a>
                        </li>
                        <li>
                            <a href="#InfluencerPane" uk-scroll>
                                influencer
                            </a>
                        </li>
            
                    </ul>
            
            </div>
            @endif

        </nav>
    </div>
</div>

<div id="NavOffcanvas" uk-offcanvas>
    <div class="uk-offcanvas-bar">

        <button class="uk-offcanvas-close" type="button" uk-close></button>

        <ul class="uk-nav mt-4">
                <li>
                    <a href="https://suvenia.com" target="_blank">
                        Suvenia
                    </a>
                </li>
                <li>
                    <a href="#SellerPane">
                        Seller
                    </a>
                </li>
                <li>
                    <a href="#DesignerPane">
                        Designer
                    </a>
                </li>
                <li>
                    <a href="#InfluencerPane">
                        Influencer
                    </a>
                </li>
    
            </ul>
        
    </div>
</div>