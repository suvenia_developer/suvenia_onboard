@if (session('success'))
<p id="ToastrSuccessNotice" style="display: none;">{{ session('success') }}</p>
<script type="text/javascript">
  	toastr.success($('#ToastrSuccessNotice').text());
  </script>
@endif

@if (session('error'))
<p id="ToastrErrorNotice" style="display: none;">{{ session('error') }}</p>
<script type="text/javascript">
  	toastr.error($('#ToastrErrorNotice').text());
  </script>
@endif 
