class App{
    public constructor(){
        this.set_ajax();
        this.reg_form();
    };

    public set_ajax(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    }  

    public reg_form(){
        $(document).on('click', '.__reg_form_trigger', function(e){
            e.preventDefault();
            $.ajax({
                url:  $(e.currentTarget).attr('href'),
                type: 'GET',
                data: {},
                beforeSend: function(){
                    //$(e.currentTarget).next()
                },
                timeout: 20000
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.status == 200){
                    $('body').prepend(
                        '<div id="__zone_address_modal" class="uk-modal-full" uk-modal>'+
                        data +
                        '</div>'
                    );
                    UIkit.modal("#__zone_address_modal").show();
                }
            });
        });
    }


   }
   const _app = new App();