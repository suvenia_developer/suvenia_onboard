<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $role = Role::firstOrNew(['name' => 'admin']);
        if (!$role->exists) {
            $role->fill([
                    'display_name' => __('voyager::seeders.roles.admin'),
                ])->save();
        }

        $role = Role::firstOrNew(['name' => 'user']);
        if (!$role->exists) {
            $role->fill([
                    'display_name' => __('voyager::seeders.roles.user'),
                ])->save();
        }

        $role = Role::firstOrNew(['name' => 'seller']);
        if (!$role->exists) {
            $role->fill([
                    'display_name' => __('voyager::seeders.roles.seller'),
                ])->save();
        }

        $role = Role::firstOrNew(['name' => 'designer']);
        if (!$role->exists) {
            $role->fill([
                    'display_name' => __('voyager::seeders.roles.designer'),
                ])->save();
        }

        $role = Role::firstOrNew(['name' => 'influencer']);
        if (!$role->exists) {
            $role->fill([
                    'display_name' => __('voyager::seeders.roles.influencer'),
                ])->save();
        }
    }
}
