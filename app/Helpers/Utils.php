<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Utils{
    
    public function isActive($routes = []){
        if(in_array(Route::currentRouteName(), $routes)){
            return true;
        }
        
        return false;
    }

    public function makeActive($routes = [], $class){
        if(in_array(Route::currentRouteName(), $routes)){
            return $class;
        }
        return '';
    }
     
    public function can($abilities){
        $user_perm = Auth::user()->getAbilities()->pluck('name')->toArray();
        if(array_intersect($user_perm, $abilities)){
            return true;
        }
        return false;
    }

    public function device(){
        $agent = new \Jenssegers\Agent\Agent();
        return $agent;
    }

     public function get_image($key){
        return str_replace('\\', '/', '/storage/' . setting($key));
     }
 
}