<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Mail\ThanksMail;

class HybridCtrl extends Controller
{
    public function fetch_form(Request $request, $type){
        return View::make('form', ['type'=> $type]);
    } 

    public function save_form(Request $request){
        $role = DB::table('roles')->where('name', strtolower($request->type))->first();
        $validate = Validator::make($request->all(), [
            'email' => 'email|required|unique:users',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'portfolio' => 'sometimes|required|url',
            'instagram' => 'sometimes|required|url'
        ]);

        if($validate->fails()){
            return response()->json(['errors'=> $validate->errors()], 500);
        }else{
            $user = new \App\User();
            $user->name = $this->get_username($request->email);
            $user->username = $this->get_username($request->email);
            $user->email = $request->email;
            $user->password = bcrypt($request->email);
            $user->portfolio = isset($request->portfolio) ? $request->portfolio : '';
            $user->instagram = isset($request->instagram) ? $request->instagram : '';
            if($user->save()){
                DB::table('user_roles')->insert([
                    'user_id'=> $user->id,
                    'role_id'=> $role->id
               ]);
               $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($user->email)->later($when, new ThanksMail($user, $request->type));
                    
                return response()->json(['message'=> 'Account created successfully!', 'redirect_url'=> route('success', ['token'=> encrypt(\Carbon\Carbon::now()->addMinutes(1))])], 200);
            }
        }
    }

    public function success(Request $request, $token){
        if(!$token){
            return redirect()->route('home')->with('error', 'Missing Token!');
        }else{
            $now = \Carbon\Carbon::now();
            $expiry = \Carbon\Carbon::parse(decrypt($token));
            if($expiry <= $now){
                return redirect()->route('home')->with('error', 'Token expired!');
            }
        }
        return view('success');
    }

    public function get_username($email){
       $data = explode("@", $email);
       $username = $data[0];
       return $username;
    }



}
